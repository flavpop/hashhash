import java.util.List;
import java.util.Objects;

/**
 * Created by Pop on 01/03/2018.
 */
public class Car {
    public Point position;
    public List<Ride> rides;
    public int currentTime;

    public Car(Point position, List<Ride> rides, int currentTime) {
        this.position = position;
        this.rides = rides;
        this.currentTime = currentTime;
    }

    @Override
    public String toString() {
        return "Car{" +
                "position=" + position +
                ", rides=" + rides +
                ", currentTime=" + currentTime +
                '}';
    }

    public void addRide(Ride ride) {
        rides.add(ride);

        int distance = Math.abs(position.x - ride.start.x) + Math.abs(position.y - ride.start.y);
        this.position.x = ride.end.x;
        this.position.y = ride.end.y;
        int rideDistance = Math.abs(ride.end.x - ride.start.x) + Math.abs(ride.end.y - ride.start.y);
        this.currentTime += distance;
        this.currentTime += rideDistance;

    }
}

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {

    //Sper cei care aveti linux sa nu fie probleme aici
    private static String filePath = "src\\main\\resources\\";
    //  private static String fileIn = "a_example";
    // private static String fileIn2 = "b_should_be_easy";
    // private static String fileIn2 = "d_metropolis";
    // private static String fileIn2 = "e_high_bonus";
    private static String fileInExtension = ".in";
    private static String fileOutExtension = ".out";
    private static List<Car> cars = new ArrayList<Car>();
    private static int steps;
    private static int bonus;
    private static int rows;
    private static int cols;
    private static List<Ride> rides;

    public static void main(String[] args) throws IOException {
        String[] files = new String[]{"a_example", "b_should_be_easy", "c_no_hurry", "d_metropolis", "e_high_bonus"};

        for (String s : files) {
            cars = new ArrayList<Car>();
            rides = new ArrayList<Ride>();
            readInput(s);

            // Flavia
            simpleResult(s);
            //Andrei
//            complexResult(s);
            simpleResult2(s);
        }

    }

    public static void simpleResult(String fileName) {
        while (!rides.isEmpty()) {
            for (Car c : cars) {
                if (rides.isEmpty()) break;
                c.rides.add(rides.get(0));
                rides.remove(0);
            }
        }
        printOutput();
        try {
            write(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void simpleResult2(String fileName) {
        while (!rides.isEmpty()) {
            for (Car c : cars) {
                rides.sort(  new Comparator<Ride>() {
                    @Override
                    public int compare(Ride ride1, Ride ride2) {

                        return distance(ride1) - distance(ride2) + (startTime(ride1, ride2));
                    }


                    private int startTime(Ride ride1, Ride ride2) {
                        return ride1.earliestStart - ride2.earliestStart;
                    }

                    private int distance(Ride ride1) {
                        return Math.abs(ride1.start.x - c.position.x) + Math.abs(ride1.start.y- c.position.y);
                    }

                });

                if (rides.isEmpty()) break;
                c.rides.add(rides.get(0));
                rides.remove(0);
            }
        }
        printOutput();
        try {
            write(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void complexResult(String fileName) throws IOException {
        for (Ride ride : rides) {
            int index = 0;
            int maxGrade = 0;
            for (int i = 0; i < cars.size(); i++) {
                int grade = ride.getPoints(cars.get(i));
                if (grade > maxGrade) {
                    maxGrade = grade;
                    index = i;
                }
            }
            cars.get(index).addRide(ride);
        }
        printOutput();
        write(fileName);
    }

    public static void printOutput() {
        for (Car c : cars) {
            System.out.print(c.rides.size());
            System.out.print(" ");
            for (Ride r : c.rides) {
                System.out.print(r.index);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    private static void readInput(String fileName) {
        BufferedReader br;
        FileReader fr;

        List<Car> assigendRidesToCards = new ArrayList<Car>();
        try {
            fr = new FileReader(filePath + fileName + fileInExtension);
            br = new BufferedReader(fr);
            Scanner s = new Scanner(br);
            rows = s.nextInt();
            cols = s.nextInt();
            int nrOfVehicles = s.nextInt();

            for (int i = 0; i < nrOfVehicles; i++) {
                cars.add(new Car(new Point(0, 0), new ArrayList<Ride>(), 0));
            }
            int nrOfRides = s.nextInt();

            Ride.bonus = s.nextInt();

            steps = s.nextInt();
            rides = new ArrayList<Ride>();
            for (int i = 0; i < nrOfRides; i++) {
                Ride ride = new Ride();
                ride.index = i;
                ride.start.x = s.nextInt();
                ride.start.y = s.nextInt();
                ride.end.x = s.nextInt();
                ride.end.y = s.nextInt();
                ride.earliestStart = s.nextInt();
                ride.latestFinish = s.nextInt();
                rides.add(ride);
            }

            // for (int i = 0; i < rides.size(); i++) {
            //     System.out.println(rides.get(i).earliestStart);
            // }

            rides.sort(
                    new Comparator<Ride>() {
                        @Override
                        public int compare(Ride ride1, Ride ride2) {
                            return ride1.earliestStart - ride2.earliestStart;

                        }
                    }
            );

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static void write(String fileName) throws IOException {
        BufferedWriter outputWriter;
        outputWriter = new BufferedWriter(new FileWriter(fileName));
        for (Car c : cars) {
            outputWriter.write(Integer.toString(c.rides.size()));
            outputWriter.write(" ");
            for (Ride r : c.rides) {
                outputWriter.write(Integer.toString(r.index));
                outputWriter.write(" ");
            }
            outputWriter.write("\n");
        }
        outputWriter.flush();
        outputWriter.close();
    }
}

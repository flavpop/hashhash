public class Ride {
    public Point start = new Point();
    public Point end = new Point();
    public int earliestStart;
    public int latestFinish;
    public static int bonus;
    public int index;

    public int getPoints(Car car) {
        int grade = 0;
        int distance = Math.abs(start.x - car.position.x) + Math.abs(start.y - car.position.y);
        int routeDistance = Math.abs(start.x - end.x) + Math.abs(start.y - end.y);

        int startArrival = car.currentTime + distance;
        int destArrival = startArrival + routeDistance;

        if (destArrival <= latestFinish) {
            grade += distance;
            if (destArrival <= latestFinish)
                grade += bonus;
        }
        return (int)Math.round ((double) grade*10 / distance);
    }

    @Override
    public String toString() {
        return "Ride{" +
                "start=" + start +
                ", end=" + end +
                ", earliestStart=" + earliestStart +
                ", latestFinish=" + latestFinish +
                ", index=" + index +
                '}';
    }
}
